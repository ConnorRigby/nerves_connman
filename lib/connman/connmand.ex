defmodule Connman.Connmand do
  @moduledoc "Handles the daemon process of connman."
  use GenServer
  require Logger

  #TODO(Connor)
  # This app should be able to exist without starting the conmand daemon.
  # It should have the ability to start if neede, but if already running,
  # or started externally we shouldn't start this module.

  @doc false
  def start_link(connmand_exec) do
    GenServer.start_link(__MODULE__, [connmand_exec], [name: __MODULE__])
  end

  ## GenServer Callbacks

  defmodule State do
    @moduledoc false
    defstruct [:port]
  end

  def init([nil]) do
    {:stop, :connman_not_found}
  end

  @dbus_config_file """
  <!DOCTYPE busconfig PUBLIC "-//freedesktop//DTD D-Bus Bus Configuration 1.0//EN"
   "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
  <busconfig>
  <listen>tcp:host=0.0.0.0,port=3000</listen>
  <listen>unix:tmpdir=/tmp</listen>
  <auth>ANONYMOUS</auth>
  <allow_anonymous/>
  </busconfig>
  """

  def init([exec]) do
    #TODO(Connor) - This stuff needs to happen in some other package.
    :ok = File.mkdir_p!("/var/run/dbus/")
    File.write!("/tmp/dbus_system.conf", @dbus_config_file)
    {_, 0} = System.cmd("dbus-daemon", ~w(--system --fork --nosyslog), into: IO.stream(:stdio, :line))
    {_, 0} = System.cmd("dbus-daemon", ~w(--config-file=/tmp/dbus_system.conf --fork --print-address --nosyslog), into: IO.stream(:stdio, :line))

    port = Port.open({:spawn_executable, exec}, [{:args, ~w(-n)}, :binary, :exit_status, :use_stdio, :stderr_to_stdout])
    {:ok, struct(State, [port: port])}
  end

  def handle_info({_port, {:data, data}}, state) do
    # IO.puts "----\n\n"
    # IO.puts data
    # IO.puts "\n\n----"
    Logger.info data
    {:noreply, state}
  end

  def handle_info({_port, {:exit_status, 0}}, state) do
    Logger.info "Connmand exited in a normal state."
    {:stop, :normal, state}
  end

  def handle_info({_port, {:exit_status, status}}, state) do
    Logger.error "Connmand exited."
    {:stop, {:port_exit, status}, state}
  end
end
