defmodule Connman.IfaceSupervisor do
  @moduledoc "Supervises individual interfaces."
  use Supervisor

  @doc false
  def start_link do
    Supervisor.start_link(__MODULE__, [], [name: __MODULE__])
  end

  def init([]) do
    children = []
    supervise(children, [strategy: :one_for_one])
  end
end
