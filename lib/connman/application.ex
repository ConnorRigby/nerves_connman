defmodule Connman.Application do
  @moduledoc false
  use Supervisor

  @doc false
  def start(_type, _args) do
    Supervisor.start_link(__MODULE__, [], [name: __MODULE__])
  end

  @doc false
  def init([]) do
    children = [
      supervisor(Connman.Connmand, [System.find_executable("connmand")]),
      supervisor(Connman.IfaceSupervisor, [])
    ]
    supervise(children, [strategy: :one_for_all])
  end
end
