defmodule Connman.Interface do
  @moduledoc "Individual interface monitor."
  require Logger
  use DBux.PeerConnection

  def start_link(ifname, args) do
    DBux.PeerConnection.start_link(__MODULE__, [ifname | args], [name: process_name(ifname)])
  end

  def process_name(ifname) do
    Module.concat(__MODULE__, ifname)
  end

  def init([ifname | args]) do
    hostname = "0.0.0.0"
    initial_state = %{ifname: ifname}
    {:ok, "tcp:host=" <> hostname <> ",port=3000", [:anonymous], initial_state}
  end

  def handle_up(state) do
    Logger.info "#{state.ifname} => DBUS UP"
    {:noreply, state}
  end

  def handle_down(state) do
    Logger.info "#{state.ifname} => DBUS DOWN"
    {:noreply, state}
  end

end
