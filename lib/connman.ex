defmodule Connman do
  @moduledoc """
  Some Useful information about using this library.
  """
  alias Connman.{Interface, IfaceSupervisor}

  require Logger
  import Supervisor.Spec

  def setup(ifname, settings) when is_list(settings) do
    Supervisor.start_child(IfaceSupervisor, worker(Interface, [ifname, settings], [restart: :permanent, id: Interface.process_name(ifname) ]))
  end

  def teardown(ifname) do
    proc_name = Interface.process_name(ifname)
    case lookup(proc_name) do
      {:ok, child_id} ->
        :ok = Supervisor.terminate_child(IfaceSupervisor, child_id)
        :ok = Supervisor.delete_child(IfaceSupervisor, child_id)
      {:error, reason} -> {:error, reason}
    end
  end

  def status(ifname) do
    proc_name = Interface.process_name(ifname)
    case lookup(proc_name) do
      {:ok, _child_id} ->
        Interface.status(proc_name)
      {:error, reason} -> {:error, reason}
    end
  end

  defp lookup(proc_name) do
    with pid when is_pid(pid) <- Process.whereis(proc_name),
      children when is_list(children) <- Supervisor.which_children(IfaceSupervisor),
      {child_id, ^pid, _, _} <- Enum.find(children, &match?(^pid, elem(&1, 1))) do
        {:ok, child_id}
      else
        _ -> {:error, :interface_not_found}
      end
  end

end
