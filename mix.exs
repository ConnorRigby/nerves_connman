defmodule Connman.Mixfile do
  use Mix.Project

  def project do
    [
      app: :connman,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Connman.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dbux, "~> 1.0.0"},
      {:connection, "~> 1.0.2"}
    ]
  end
end
